<?php
/**
 * @var $module Dudley\Patterns\Pattern\Carousel\Carousel
 * @var $item   Dudley\Patterns\Abstracts\AbstractCarouselItem
 */
?>

<section class="carousel">
	<!-- Carousel Content -->
	<ul class="carousel__items" <?php $module->autoplay_attr(); ?> <?php $module->autoplay_speed_attr(); ?>>
		<?php foreach ( $module->get_items() as $i => $item ) : ?>
			<li class="carousel__item" <?php $item->image_attributes( $i ); ?>>
				<p class="screen-reader-text"><?php $item->img_alt(); ?></p>
			</li>
		<?php endforeach; ?>
	</ul>

	<!-- Step Controls -->
	<button class="carousel__step-btn carousel__step-btn--prev">
		<span class="carousel__btn-text"><?php esc_html_e( 'Previous Image', 'dudley' ); ?></span>
	</button>
	<button class="carousel__step-btn carousel__step-btn--next">
		<span class="carousel__btn-text"><?php esc_html_e( 'Next Image', 'dudley' ); ?></span>
	</button>

	<!-- Pause Controls -->
	<?php if ( $module->get_autoplay() ) : ?>
		<button class="carousel__pause" aria-label="Pause Carousel">
			<span class="carousel__pause-text"><?php esc_html_e( 'Pause', 'dudley' ); ?></span>
		</button>
	<?php endif; ?>

	<!-- Jump Controls -->
	<?php if ( $module->has_jump_nav() ) : ?>
		<nav class="carousel__jump-nav">
			<?php foreach ( $module->get_items() as $i => $item ) : ?>
				<button class="carousel__jump-btn" <?php $item->img_index_attr( $i ); ?>></button>
			<?php endforeach; ?>
		</nav>
	<?php endif; ?>
</section><!-- .carousel -->
