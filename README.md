# Dudley Patterns Framework: Carousel Module
This package is designed for use with the Dudley plugin. You
can include it in your project by requiring this repository in the 
Dudley plugin's `composer.json` file.

In the `require` object of `composer.json`, add this line:
```
"dudley\carousel": "dev-master"
```

## How to Use in Template Files
Like all modules in the Dudley Patterns Framework, action names are generated from
both the object's `$meta_type` property and its `$action_name` property.
The format is `dudley_{$meta_type}_{$action_name}`.

Thus, if you are using Advanced Custom Fields, the code to include in your
template file to render the carousel would be:
`<?php do_action( 'dudley_acf_carousel' ); ?>`

## Available Filters
* `dudley_carousel_item_image_size`: Alter the default returned size for each image.

## How to Override the Markup in Your Own Template File
1. If none exists, create a directory in your theme named `dudley-modules`
2. Add a file named `carousel.php` in the `dudley-modules` directory.
