<?php
namespace Dudley\Patterns\Pattern\Carousel;

use Dudley\Patterns\Abstracts\AbstractCarouselItem;

/**
 * Class CarouselItem
 *
 * @package Dudley\Patterns\Pattern\Carousel
 */
class CarouselItem extends AbstractCarouselItem {
	/**
	 * CarouselItem constructor.
	 *
	 * @param $image
	 */
	public function __construct( $image ) {
		parent::__construct( $image, 'large', Carousel::$action_name );
	}
}
