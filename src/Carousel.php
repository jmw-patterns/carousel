<?php
namespace Dudley\Patterns\Pattern\Carousel;

use Dudley\Patterns\Abstracts\AbstractCarousel;

/**
 * Class Carousel
 *
 * @package Dudley\Patterns\Pattern\Carousel
 */
class Carousel extends AbstractCarousel {
	/**
	 * Base name for the module's action.
	 *
	 * @var string
	 */
	public static $action_name = 'carousel';

	/**
	 * Carousel constructor.
	 *
	 * @param $autoplay
	 * @param $autoplay_speed
	 * @param $jump_nav_show
	 */
	public function __construct( $autoplay, $autoplay_speed, $jump_nav_show ) {
		parent::__construct( $autoplay, $autoplay_speed, $jump_nav_show );
	}
}
