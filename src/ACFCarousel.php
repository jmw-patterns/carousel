<?php
namespace Dudley\Patterns\Pattern\Carousel;

/**
 * Class ACFCarousel
 *
 * @package Dudley\Patterns\Pattern\Carousel
 */
class ACFCarousel extends Carousel {
	/**
	 * @var string
	 */
	public static $meta_type = 'acf';

	/**
	 * ACFCarousel constructor.
	 */
	public function __construct() {
		if ( ! get_field( 'carousel_items' ) ) {
			return;
		}

		while ( has_sub_field( 'carousel_items' ) ) {
			$this->add_item(
				new CarouselItem( get_sub_field( 'carousel_item_image' ) )
			);
		}

		parent::__construct(
			get_field( 'carousel_autoplay' ),
			get_field( 'carousel_autoplay_speed' ),
			get_field( 'carousel_jump_nav_show' )
		);
	}
}
